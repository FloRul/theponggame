/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

/**
 *
 * @author Florian
 */
public class ThePongGame {
    
    @SuppressWarnings("empty-statement")
    public static void main(String[] argv) throws LWJGLException{
        Pong p = new Pong();
        p.init();
        while(!Display.isCloseRequested()){
            p.run();
        }
        Display.destroy();
        
    }
}
