/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29.GlobalClass;

import lwjgl29.Pong;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

/**
 *
 * @author Florian
 */
public class Palet extends Quad{
    private int Side;
    protected float speed = 0.3f;
    
    public Palet(float _x, float _y, int _width, int _height, float[] rgb, int _side, Pong _pong) {
        super(_x, _y, _width, _height, rgb, _pong);
        this.Side = _side;
    }
    
    public int getSide(){
        return this.Side;
    }
    
    @Override
    public void move(){
        switch(this.Side){
            case 1 : 
                if (Keyboard.isKeyDown(Keyboard.KEY_S)) this.y -= this.speed;
                if (Keyboard.isKeyDown(Keyboard.KEY_Z)) this.y += this.speed;
            break;
            
            case 2 :
                if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) this.y -= this.speed;
                if (Keyboard.isKeyDown(Keyboard.KEY_UP)) this.y += this.speed;
            break;
            
            case 0 : 
                /*
                Joueur IA
                */
        }
        if(this.y>=Display.getHeight()-this.height) this.y = Display.getHeight()-this.height;
        if(this.y<=0)this.y = 0;
    }
    
    public float getMiddleY(){
        return this.y+this.height/2;
    }
}
