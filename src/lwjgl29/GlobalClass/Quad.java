/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29.GlobalClass;

import lwjgl29.Pong;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Florian
 */
public abstract class Quad extends Item{
    protected int width,height;
    protected float x,y;
    protected float r,g,b;
    protected Pong pong;

    public Quad(float _x, float _y, int _width, int _height, float[] rgb, Pong _pong){
        this.height = _height;
        this.width = _width;
        this.x = _x;
        this.y = _y;
        this.r = rgb[0];
        this.g = rgb[1];
        this.b = rgb[2];
        this.pong = _pong;
    }
    
    @Override
    public void init(){
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Display.getWidth(), 0, Display.getHeight(), 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
    }
    
    @Override
    public void clear(){
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
    }
    
    @Override
    public void draw(){
        GL11.glColor3f(this.r, this.g, this.b);
        GL11.glBegin(GL11.GL_POLYGON);
        //position de départ
            GL11.glVertex2f(this.x,this.y);
            //dessin des 3 cotés
            GL11.glVertex2f(this.x+this.width,this.y);
            GL11.glVertex2f(this.x+this.width,this.y+this.height);
            GL11.glVertex2f(this.x,this.y+this.height);
        GL11.glEnd();
        this.move();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
    
    public float getTop(){
        return this.y+this.height;
    }
    
    public float getRight(){
        return this.x+this.width;
    }
    
    public abstract void  move();
}
