/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29.GlobalClass;

/**
 *
 * @author Florian
 */
public abstract class Item {
    //Un item a besoin de ces 3 méthodes pour fonctionner
    public abstract void draw();
    public abstract void init();
    public abstract void clear();
}
