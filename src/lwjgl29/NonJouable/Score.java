/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29.NonJouable;

import lwjgl29.GlobalClass.Item;
import org.lwjgl.opengl.Display;


/**
 *
 * @author Florian
 */
public class Score extends Item{
    protected String score = "";
    protected int scoreJ1, scoreJ2;
    
    @Override
    public void draw() {
        this.majScore();
        Display.setTitle(score);
    }

    @Override
    public void init() {
        this.scoreJ1 = this.scoreJ2 = 0;
    }

    @Override
    public void clear() {
    }

    public void majScore(){
        this.score = "SCORE : " + this.scoreJ1 + " : " + this.scoreJ2;
    }
}
