/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29.NonJouable;

import lwjgl29.GlobalClass.Palet;
import lwjgl29.GlobalClass.Quad;
import lwjgl29.Pong;
import org.lwjgl.opengl.Display;

/**
 *
 * @author Florian
 */
public class Ball extends Quad{
    protected Pong _pong;
    protected float dirRad; 
    protected float speed;
    
    public Ball(float _x, float _y, float[] rgb, float _dirRad, Pong pong) {
        super(_x, _y, 20, 20, rgb, pong);
        this.dirRad = _dirRad;
        this.speed  = 0.3f;
        this._pong = pong;
    }

    @Override
    public void move() {
        //collisions bord gauche ou droit écran
        if(this.x<=0){
            this._pong.getGameScore().scoreJ2+=1;
            this.resetPos();
        }
        if(this.getRight()>=Display.getWidth()){
            this._pong.getGameScore().scoreJ1+=1;
            this.resetPos();
        }
        
        //collisoin bord haut et bas
        if(this.getTop()>=Display.getHeight() || this.y<=0)this.dirRad = (float)Math.PI*2 - this.dirRad;
        //Gestion collision joueurs
        this.collisionManager(this._pong.getJoueur1());
        this.collisionManager(this._pong.getJoueur2());
        //Et on le bouge
        this.moveDir();
    }
    
    public void moveDir(){
        this.x+=this.speed*Math.cos(this.dirRad);
        this.y+=this.speed*Math.sin(this.dirRad);
    }
    
    public void resetPos(){
        this.x = Display.getWidth()/2;
        this.y = Display.getHeight()/2;
        /*
        sequence de lancement de balle 
        +
        MàJ des scores
        */
        float tempRad = (float) (Math.random()*2*Math.PI);
        if((tempRad >= Math.PI/4 && tempRad <= 3*Math.PI/4) || (tempRad >= 5*Math.PI/4 && tempRad <= 7*Math.PI/4)){
            tempRad+=Math.PI/2;
        }
        this.dirRad = tempRad;
    }
    
    public void TestPalet(Palet p){
        System.out.println(p.getX());
    }
    
    public void collisionManager(Palet p){
        //tolérance à la moitié de la balle
        int tolerance = this.height/2;
        int relativePos = (int)(this.getMiddleY()-p.getY());
        switch(p.getSide()){
            case 1 :
                if(this.x<=p.getRight() && this.y>=p.getY()-tolerance && this.getTop()<=p.getTop()+tolerance && p.getSide()==1){
                    if(this.getMiddleY()<=p.getTop()+tolerance && this.getMiddleY()>=p.getY()){
                        this.dirRad =(float) (7*Math.PI/4+relativePos*((Math.PI/(2*100))));
                    }
                }
            break;
            
            case 2 :
                if((this.getRight()>=p.getX()) && this.y>=p.getY()-tolerance && this.getTop()<=p.getTop()+tolerance && p.getSide()==2){
                    if(this.getMiddleY()<=p.getTop()+tolerance && this.getMiddleY()>=p.getY()){
                        this.dirRad =(float) (5*Math.PI/4+relativePos*(-(2*Math.PI/(4*100))));
                    }
                }
            break;
        }
    }
        
    public float getMiddleY(){
        return this.y+this.height/2;
    }   
}
