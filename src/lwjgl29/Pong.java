/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29;

import lwjgl29.GlobalClass.Item;
import lwjgl29.NonJouable.Ball;
import java.util.ArrayList;
import lwjgl29.GlobalClass.Palet;
import lwjgl29.NonJouable.Score;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

/**
 *
 * @author Florian
 */
public class Pong {
    ArrayList<Item> Items;
    Palet Joueur1, Joueur2;
    Score gameScore;
    Ball ball;
    
    public Pong() {}
    
    public void init() throws LWJGLException{
        //On commence par créer l'écran
        this.Items = new ArrayList();
        DisplayInst d = new DisplayInst(800,600);
        Display.sync(50);
        d.start();
        //On crée les objets du jeu
        float rgb[] = {1.0f,1.0f,1.0f};
        Palet j1 = new Palet(0,0,20,100,rgb,1,this);
        Palet j2 = new Palet(Display.getWidth()-20,0,20,100,rgb,2,this);
        this.Joueur1 = j1;
        this.Joueur2 = j2;
        Ball b = new Ball(Display.getWidth()/2, Display.getHeight()/2,rgb,(float)(Math.PI/4), this);
        //Ball b2 = new Ball(Display.getWidth()/2, Display.getHeight()/2,rgb,(float)(5*Math.PI/4), this);
        this.gameScore = new Score();
        //On les intègre à la liste des items du jeu
        this.Items.add(j1);
        this.Items.add(j2);
        this.Items.add(b);
        //this.Items.add(b2);
        this.Items.add(this.gameScore);
        //On initialise tous les items 
        Items.stream().forEach((i) -> {i.init();});
    }
    
    public void run(){
        Items.stream().forEach((i) -> {i.draw();});
        Display.update();
        Items.stream().forEach((i) -> {i.clear();});
    }

    public Palet getJoueur1() {return Joueur1;}
    public Palet getJoueur2() {return Joueur2;}

    public ArrayList<Item> getItems() {
        return Items;
    }

    public Ball getBall() {
        return ball;
    }
    
    public Score getGameScore() {return gameScore;}
}
