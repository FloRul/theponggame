/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lwjgl29;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

/**
 *
 * @author Florian
 */
public class DisplayInst {

    public DisplayInst() {
    }
    
    public DisplayInst(int width, int height) throws LWJGLException {
        Display.setDisplayMode(new DisplayMode(width,height));
    }
    
    public void start(){
        try {
            Display.create();
        } catch (LWJGLException ex) {
            Logger.getLogger(DisplayInst.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
