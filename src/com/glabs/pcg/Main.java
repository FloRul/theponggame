package com.glabs.pcg;
 
import java.util.Random;
 
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;
 
public class Main
{
    //Doit-on continuer le programme
    private boolean done = false;
 
    //Titre de la fenetre
    private final String windowTitle = "Heightmap";
 
    //Taille de la map
    int precision = 65;//Doit être de la forme 2^n + 1
 
    //Rugosité du terrain
    float rugosite = 0.005f;
 
    //Les vertex composant la map
    float[][] map = new float[precision][precision];
 
    //La position de la caméra
    Vector3f camPos = new Vector3f();
 
    //L'angle de la caméra par rapport au centre de la map
    float angle = (float) (Math.PI / 4);
 
    //La distance de la camera par rapport au centre de la map
    float distanceVue = 1f;
 
    //Le mode d'affichage
    private DisplayMode displayMode;
 
    /**
     * Fonction d'entrée du programme
     * @param args
     */
    public static void main(String args[])
    {
        Main app = new Main();
        app.run();
    }
 
    /**
     * Démarrage du programme
     */
    public void run()
    {
        try
        {
            //On initialise le programme (OpenGL principalement)
            init();
 
            //La boucle d'execution
            while(!done)
            {
                update();
                render();
 
                //On met à jour l'affichage avec nos nouvelles données
                Display.update();
            }
 
            //On detruit l'affichage puisqu'on a termine
            Display.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(0);
        }
    }
 
    /**
     * La fonction dans laquelle s'effectue la logique du programme
     */
    private void update()
    {
 
        /*
         * L'utilisateur veut-il quitter ?
         */
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
            done = true;
 
        if(Display.isCloseRequested())
            done = true;
 
        /*
         * Touches de déplacement
         */
        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
            camPos.y += 0.0004f;
        else if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
            camPos.y -= 0.0004f;
 
        if(Keyboard.isKeyDown(Keyboard.KEY_S))
            distanceVue += 0.0004f;
        else if(Keyboard.isKeyDown(Keyboard.KEY_Z))
            distanceVue -= 0.0004f;
 
        if(distanceVue < 0)
            distanceVue = 0;
 
        if(Keyboard.isKeyDown(Keyboard.KEY_D))
            angle -= 0.0005f;
        if(Keyboard.isKeyDown(Keyboard.KEY_Q))
            angle += 0.0005f;
 
        /*
         * Rafraichissement de la map et modification de sa dureté/rugosité
         */
        if(Keyboard.isKeyDown(Keyboard.KEY_R))
            creerMapDiamantsCarre();
 
        int dWheel = Mouse.getDWheel();
        if(dWheel < 0)
            rugosite -= 0.0001;
        else if(dWheel > 0)
            rugosite += 0.0001;
 
        if(dWheel != 0)
        {
            System.out.println("rugosite: " + rugosite);
            creerMapDiamantsCarre();
        }
 
        if(rugosite < 0)
            rugosite = 0;
        else if(rugosite > 2)
            rugosite = 2;
    }
 
    /**
     * Fait le rendu de la map en fonction des angles et distance que l'on utilise
     * @return
     */
    private void render()
    {
        //Efface l'écran et le buffer de profondeur
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
 
        //Reset de la matrice ModelView
        GL11.glLoadIdentity();
 
        //On modifie la position de la caméra
        camPos.x = (float) (Math.cos(angle) * distanceVue) + .5f;
        camPos.z = (float) (Math.sin(angle) * distanceVue) + .5f;
        GLU.gluLookAt(camPos.x, camPos.y, camPos.z, .5f, 0f, .5f, 0, 1, 0);
 
        //La couleur de notre map (blanc)
        GL11.glColor3f(1f, 0.8f, 0.6f);
 
        //On dessine sous forme de triangles
        GL11.glBegin(GL11.GL_TRIANGLES); // Drawing Using Triangles
        for(int x = 0; x < precision - 1; x++)
        {
            for(int y = 0; y < precision - 1; y++)
            {
                //Les triangles bas-gauche de nos carrés
                GL11.glVertex3f(x / (float) precision, map[x][y], y / (float) precision);
                GL11.glVertex3f((x + 1) / (float) precision, map[x + 1][y], y / (float) precision);
                GL11.glVertex3f(x / (float) precision, map[x][y + 1], (y + 1) / (float) precision);
 
                //Les triangles haut-droit de nos carrés
                GL11.glVertex3f((x + 1) / (float) precision, map[x + 1][y], y / (float) precision);
                GL11.glVertex3f((x + 1) / (float) precision, map[x + 1][y + 1], (y + 1) / (float) precision);
                GL11.glVertex3f(x / (float) precision, map[x][y + 1], (y + 1) / (float) precision);
            }
        }
        GL11.glEnd();
    }
 
    /**
     * Cree la fenetre LWJGL
     * @throws Exception
     */
    private void createWindow() throws Exception
    {
        //Permet de choisir le DisplayMode correspondant à du 640*480 avec 4 octets par pixel
        DisplayMode d[] = Display.getAvailableDisplayModes();
        for (DisplayMode d1 : d) {
            if (d1.getWidth() == 640 && d1.getHeight() == 480 && d1.getBitsPerPixel() == 32) {
                displayMode = d1;
                break;
            }
        }
 
        //Reglage de l'affichage
        Display.setDisplayMode(displayMode);
        Display.setTitle(windowTitle);
        Display.create();
    }
 
    /**
     * Initialise notre programme
     * @throws Exception
     */
    private void init() throws Exception
    {
        createWindow();
        initGL();
        creerMapDiamantsCarre();
    }
 
    /**
     * Creation de la map avec l'algorithme Diamants/Carres
     */
    private void creerMapDiamantsCarre()
    {
        //Les quelques variables/objets qui serviront à la creation de la map
        int espace = (int) precision;
        int largeur = (int) precision;
        Random r = new Random();
 
        /*
         * On place les coins de notre map à une hauteur précise, puis que les autres
         * points seront crées à partir de ces quatres points pré-existants.
         */
        float hauteurBase = 0f;
        map[0][0] = hauteurBase;
        map[0][precision-1] = hauteurBase;
        map[precision-1][precision-1] = hauteurBase;
        map[precision-1][0] = hauteurBase;
 
        //Tant qu'on a pas modifié tout nos points
        while(espace > 1)
        {
            /*
             * Cette echelle permet de determiner de quelle quantite maximum on va modifier le point testé
             * elle décroit au fur et à mesure que l'on diminue la taille de nos carrés/diamants
             * afin que la map paraissent douce et non totalement aléatoire
             */
            float scale = espace * rugosite;
 
            /*
             * Pour les carrés
             */
            for(int x = espace/2; x < largeur - 1; x += espace)
            {
                for(int y = espace/2; y < largeur - 1; y += espace)
                {
                    //Moyenne des hauteurs des quatres points situés dans les coins autour de ce point
                    float average = (map[x-espace/2][y-espace/2] + map[x+espace/2][y-espace/2] + map[x+espace/2][y+espace/2] + map[x-espace/2][y+espace/2]) / 4f;
 
                    //La hauteur du point depend de la moyenne de ses voisins et d'une valeur aléatoire dont le maximum est 'scale'
                    map[x][y] = average + r.nextFloat() * 2 * scale - scale;
                }
            }
 
            /*
             * Pour les "diamants"
             */
            //Variable qui sert à déterminer si la n-ieme ligne qu'on traite est paire (pas si y est pair)
            boolean estPaire = true;
            for(int y = 0; y < largeur; y += espace/2)
            {
                for(int x = (estPaire) ? espace/2 : 0; x < largeur; x = (espace%2==0) ? x + espace : x + espace - 1)
                {
                    //On fait une moyenne, mais on ne prend que des points existants. D'ou le compteur de points 'ptCount'
                    float ave = 0f;
                    int ptCount = 0;
 
                    if(x > 0)
                    {
                        ave += map[x-espace/2][y];
                        ptCount++;
                    }
                    if(y > 0)
                    {
                        ave += map[x][y-espace/2];
                        ptCount++;
                    }
                    if(x < largeur - 1)
                    {
                        ave += map[x+espace/2][y];
                        ptCount++;
                    }
                    if(y < largeur - 1)
                    {
                        ave += map[x][y+espace/2];
                        ptCount++;
                    }
 
                    ave /= ptCount;
 
                    //On applique cette moyenne sur le point courant, avec le meme systeme d'aléas que pour les carrés
                    map[x][y] = ave + r.nextFloat() * 2 * scale - scale;
                }
 
                estPaire = !estPaire;
            }
 
            espace /= 2;
        }
    }
 
    /*
     * Initialise OpenGL
     */
    private void initGL()
    {
        //GL11.glEnable(GL11.GL_TEXTURE_2D); // Enable Texture Mapping
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //Fond noir
        GL11.glClearDepth(1.0); //Reglage du buffer de profondeur
        GL11.glDepthFunc(GL11.GL_LEQUAL); //Le type de test de profondeur
 
        //Selectionne et reset la matrice de projection
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
 
        //Active la vue 'wireframe'
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
 
        //Calcul de l'Aspect Ratio de notre fenetre
        GLU.gluPerspective(45.0f, (float) displayMode.getWidth() / (float) displayMode.getHeight(), 0.1f, 100.0f);
 
        //Reglage de la position de la caméra
        camPos.x = (float) (Math.cos(angle) * distanceVue);
        camPos.y = .5f;
        camPos.y = (float) (Math.sin(angle) * distanceVue);
 
        //Selectionne la matrice ModelView
        GL11.glMatrixMode(GL11.GL_MODELVIEW); 
 
        //Censé améliorer les calculs de perspective
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
    }
}